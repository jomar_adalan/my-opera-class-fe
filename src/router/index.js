import Vue from 'vue'
import Router from 'vue-router'

import Composers from '@/components/Composers'
import Repertoire from '@/components/Repertoire'
import Settings from '@/components/Settings'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Composers',
            component: Composers
        },
        {
            path: '/Repertoire',
            name: 'Repertoire',
            component: Repertoire
        },
        {
            path: '/Settings',
            name: 'Settings',
            component: Settings
        }
    ]
})